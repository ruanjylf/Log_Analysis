# gunicorn 配置文件

import multiprocessing

bind = '0.0.0.0:8000'  # 绑定ip和端口号
# workers = multiprocessing.cpu_count() * 2 + 1  # 进程数
workers = 4
loglevel = 'info'  # 日志等级
accesslog = "/home/scy/Log_Analysis/logs/log_analysis.log"  # 访问日志文件
errorlog = "/home/scy/Log_Analysis/logs/log_analysis.error.log"  # 错误日志文件
