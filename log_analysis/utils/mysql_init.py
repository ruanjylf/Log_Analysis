import MySQLdb

conn = MySQLdb.Connect(host='127.0.0.1', port=3306, user='root', passwd='mysql', db='log_analysis', charset='utf8')
# 测试数据库
# conn = MySQLdb.Connect(host='192.168.1.238', port=3306, user='root', passwd='123456', db='log_analysis', charset='utf8')

cursor = conn.cursor()


def insert_site():
    """
    添加站点数据, 初始化数据
    """
    sql = """INSERT INTO tb_site(site_name, host, access_log, error_log, link) 
    VALUES ('%s', '%s', '%s', '%s', '%s')""" % (
    "帝诺雷斯", "lbcwx.lingxiu.top", "lbcwx.lingxiu.top.log", "lbcwx.lingxiu.top.error.log", "https://www.baidu.com")

    # 保存到数据库
    try:
        cursor.execute(sql)
        conn.commit()
    except Exception as e:
        print(e)
        conn.rollback()


if __name__ == '__main__':
    insert_site()
