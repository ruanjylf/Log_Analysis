# 自定义响应状态码


class RET:
    PARAMERR = "4000"
    NOTALL = "4001"
    PHONEERR = "4002"
    EMAILERR = "4003"
    LINKERR = "4004"
    DUTYERR = "4005"

    DBERR = "4100"
    NODATA = "4101"
    DATAEXIST = "4102"
    DATAERR = "4103"

    OK = "4200"
    SUCCESS = "4201"

    FAIL = "4300"
    FAILURE = "4301"

    REQERR = "4400"
    SESSIONERR = "4401"
    LOGINERR = "4402"
    ACCOUNTERR = "4403"
    ROLEERR = "4404"
    PWDERR = "4405"
    IPERR = "4406"

    SERVERERR = "4500"
    UNKOWNERR = "4501"
    THIRDERR = "4502"
    IOERR = "4503"


error_map = {

    # 40**
    RET.PARAMERR: u"参数有误!",
    RET.NOTALL: u"参数不全!",
    RET.PHONEERR: u"手机号码有误!",
    RET.EMAILERR: u"邮箱地址有误!",
    RET.LINKERR: u"链接网址有误!",
    RET.DUTYERR: u"值班人员不存在!",

    # 41**
    RET.DBERR: u"数据库查询错误!",
    RET.NODATA: u"未查询到数据!",
    RET.DATAEXIST: u"数据已存在!",
    RET.DATAERR: u"数据错误!",

    # 42**
    RET.OK: u"成功!",
    RET.SUCCESS: u"操作成功!",

    # 43**
    RET.FAIL: u"失败!",
    RET.FAILURE: u"操作失败!",

    # 44**
    RET.REQERR: u"非法请求!",
    RET.SESSIONERR: u"用户未登录!",
    RET.LOGINERR: u"用户登录失败!",
    RET.ACCOUNTERR: u"账号不存在!",
    RET.ROLEERR: u"用户权限不足!",
    RET.PWDERR: u"密码错误!",
    RET.IPERR: u"IP受限!",

    # 45**
    RET.SERVERERR: u"内部错误!",
    RET.UNKOWNERR: u"未知错误!",
    RET.THIRDERR: u"第三方错误!",
    RET.IOERR: u"读写错误!",

}
