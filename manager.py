# 运行文件

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from log_analysis import create_app, db
from log_analysis.models import User

# 设置运行环境
app = create_app("development")  # 开发环境
# app = create_app("production")  # 生产环境

manager = Manager(app)

# 数据库迁移
Migrate(app, db)
manager.add_command("db", MigrateCommand)


# 创建管理员账户
@manager.option("-u", "--username", dest='username')
@manager.option("-p", "--password", dest='password')
def create_admin(username, password):
    if not all([username, password]):
        app.logger.error("参数不全!")

    user = User()
    user.user_name = username
    user.password = password
    user.is_admin = True

    try:
        db.session.add(user)
        db.session.commit()
        app.logger.info("创建成功!")

    except Exception as e:
        db.session.rollback()
        app.logger.error("创建失败! {}".format(e))


if __name__ == "__main__":
    manager.run()
